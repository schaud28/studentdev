// importing dependencies
const express = require('express');
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const http = require('http');
const path = require('path');
const connectDB = require('./config/db');

// expressjs
const app = express();


connectDB();




// app.use('/api/users', require('./routes/api/users'));

// mongodb
// mongoose
//     .connect(
//         `mongodb://mongodb-service/cloudl`, {
//             useNewUrlParser: true,
//             useUnifiedTopology: true
//         })
//     .then(() => console.log("MongoDB successfully connected"))
//     .catch(err => console.log(err));


//     app.use(express.json());



app.use(express.json());


app.get('/', (req, res) => res.send("Api burning"));

app.use('/api/users', require('./routes/api/users'));
app.use('/api/auth', require('./routes/api/auth'));
app.use('/api/profile', require('./routes/api/profile'));
app.use('/api/posts', require('./routes/api/posts'));


const port = process.env.PORT || 5000;


app.listen(port, () => console.log(`Server up and running on port ${port} !`));
